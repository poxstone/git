import os
from flask import Flask

app = Flask(__name__)


APP_PORT = os.environ['APP_PORT'] if 'APP_PORT' in os.environ else '8080'
APP_VER = os.environ['APP_VER'] if 'APP_VER' in os.environ else ''

@app.route('/')
def path_root():
    return "ok {}".format(APP_VER)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=APP_PORT)

