FROM python:3.7-alpine

ENV APP_PORT=8080
ENV APP_PATH="/app"
ENV GUNICORN_MODULE="main"
ENV GUNICORN_CALLABLE="app"
ENV WORKERS=5
ENV TIMEOUT=240
ENV APP_VER="1.0L"

# app
RUN python3 -m ensurepip \
    && pip3 install --upgrade pip gunicorn flask

COPY ./ $APP_PATH

EXPOSE $APP_PORT

WORKDIR $APP_PATH
ENTRYPOINT gunicorn --workers=$WORKERS --timeout=$TIMEOUT --bind 0.0.0.0:$APP_PORT $GUNICORN_MODULE:$GUNICORN_CALLABLE;

